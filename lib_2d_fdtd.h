/******************************************************************************
 * 2-D FDTD library                               *
 * \file lib_2d_fdtd.h
*****************************************************************************/
#ifndef LIB_2D_FDTD_H_INCLUDED
#define LIB_2D_FDTD_H_INCLUDED

#include "settings.h"

void FdtdCpuError(const char *ptr_message);


/*****************************************************************************/
/* Saving data                                                               */
/*****************************************************************************/

/** Save plane to file, overwrite if file already exists.
 * \param filename name of file
 * \param data pointer to the array with data
 * \param dimx the first dimmension of the plane
 * \param dimz the second dimmension of the plane
 */
void FdtdCpuSavePlaneToFile(	const char *filename,
								const myfloat_t *data,
								const uint_t dimx,
								const uint_t dimz);

/*****************************************************************************/
/* 2-D TE mode (Hy mode) functions                                                        */
/*****************************************************************************/


/** 2-D TE (Hy mode) FDTD H-field update
 * \param dx spatial step size along X-direction
 * \param dz spatial step size along Z-direction
 * \param dt time step size
 * \param ex_field array of Ex field
 * \param ez_field array of Ez field
 * \param hy_field array of Hy field
 * \param Nx number of cells along X-direction
 * \param Nz number of cells along Z-direction
 */
void Hyfield2DFdtdUpdate(const myfloat_t dx, const myfloat_t dz, const myfloat_t dt,
						myfloat_t *ex_field, myfloat_t *ez_field, myfloat_t *hy_field,
						int Nx, int Nz);

/** 2-D TE (Hy mode) FDTD E-field update without, using ADE method
 * \param dx spatial step size along X-direction
 * \param dz spatial step size along Z-direction
 * \param dt time step size
 * \param ex_field array of Ex field
 * \param ez_field array of Ez field
 * \param hy_field array of Hy field
 * \param Nx number of cells along X-direction
 * \param Nz number of cells along Z-direction
 * \param Nwz waveguide length in cells
 * \param NInSbxup top of InSb in cells (interface between media)
 * \param NAgxup top of Ag
 * \param NAgxdown down of Ag
 * \param eps_si permittivity of Si
 * \param jx_current array of Jx current density
 * \param jz_current array of Jz current density
 * \param kp coefficient used in update of k current density (silver, ADE)
 * \param bp coefficient used in update of k current density (silver, ADE)
 * \param C1 coefficient used in update of E field (silver, ADE)
 * \param C2 coefficient used in update of E field (silver, ADE)
 * \param ex_field_next array of new values of Ex field
 * \param ez_field_next array of new values of Ez field
 * \param jx_current_next array of new values of Jx field
 * \param jz_current_next array of new values of Jz field
 * \param j_J matrix used in calculating J value based on J (InSb - anisotropic material)
 * \param j_E matrix used in calculating J value based on E (InSb - anisotropic material)
 * \param e_E matrix used in calculating E value based on E (InSb - anisotropic material)
 * \param e_H matrix used in calculating E value based on J (InSb - anisotropic material)
 */ 
void Exzfield2DFdtdUpdate(const myfloat_t dx, const myfloat_t dz, const myfloat_t dt,
						  myfloat_t *ex_field, myfloat_t *ez_field, myfloat_t *hy_field,
						  int Nx, int Nz,
						  int Nwz, int NInSbxup, int NAgxup, int NAgxdown,
						  const myfloat_t eps_si,
						  myfloat_t *jx_current, myfloat_t *jz_current, const myfloat_t kp, const myfloat_t bp, 
						  const myfloat_t C1, const myfloat_t C2,
						  myfloat_t *ex_field_next, myfloat_t *ez_field_next, myfloat_t *jx_current_next, myfloat_t *jz_current_next,
						  myfloat_t j_J[2][2], myfloat_t j_E[2][2], myfloat_t e_E[2][2], myfloat_t e_J[2][2], myfloat_t e_H[2][2]
						 );										
#endif



/*****************************************************************************/
/* 2x2 Matrices functions                                                      */
/*****************************************************************************/


/** Inverse 2x2 matrix

 * \param A original 2x2 matrix (2x2 array)
 * \param result result - inverse of A
 * | A[0][0]  A[0][1] |
 * | A[1][0]  A[1][1] |
 */
void MatrixInverse(myfloat_t A[2][2], myfloat_t result[2][2]);

/** Addition of two 2x2 matrices

 * \param A first 2x2 matrix (2x2 array)
 * \param B second 2x2 matrix (2x2 array)
 * \param result result - inverse of A
 * | A[0][0] a  A[0][1] b | * | B[0][0] e  B[0][1] f |
 * | A[1][0] c  A[1][1] d | * | B[1][0] g  B[1][1] h |
 */
void MatrixAdd(myfloat_t A[2][2], myfloat_t B[2][2],  myfloat_t result[2][2]);

/** Substraction of two 2x2 matrices
 * \param A first 2x2 matrix (2x2 array)
 * \param B second 2x2 matrix (2x2 array)
 * \param result result - inverse of A
 */
void MatrixSub(myfloat_t A[2][2], myfloat_t B[2][2],  myfloat_t result[2][2]);

/** Multiplication of two 2x2 matrices

 * \param A first 2x2 matrix (2x2 array)
 * \param B second 2x2 matrix (2x2 array)
 * \param result result - inverse of A
 * | A[0][0] a  A[0][1] b | * | B[0][0] e  B[0][1] f |
 * | A[1][0] c  A[1][1] d | * | B[1][0] g  B[1][1] h |
 */
void MatrixMulitply(myfloat_t A[2][2], myfloat_t B[2][2],  myfloat_t result[2][2]);

/** Multiplication of two 2x2 matrix by scalar
 * \param A first 2x2 matrix (2x2 array)
 * \param scalar scalar to multiply A by
 * \param result scalar * A
 */
void MatrixMultScalar(myfloat_t A[2][2], myfloat_t scalar,  myfloat_t result[2][2]);

/** Returns condition number of matrix
 * \param A first 2x2 matrix (2x2 array)
 */
myfloat_t MatrixCondNumber(myfloat_t A[2][2]);
