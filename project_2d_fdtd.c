#include "settings.h"
#include "lib_2d_fdtd.h"
#include <time.h>

#ifdef _OPENMP
#include <omp.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include <conio.h>


int main()
{

#ifdef _OPENMP
	double start, end;
#else
	clock_t start, end;
#endif
	double cpu_time_used;

	//myfloat_t freq_max=750e12;

	//myfloat_t lambda_min=c_const/freq_max;
	//myfloat_t steps_per_wavelength=20;

	myfloat_t dx = 0.1e-6;//lambda_min/steps_per_wavelength;
	myfloat_t dz = 0.5e-6;//dx;

	int Nsteps = 100000;//400000; //4e5; // 60000
	int time_e_ampl_save = 5000;

	//waveguide parameters
	myfloat_t Lwz=800e-6;		//waveguide length
	myfloat_t LAgz=5e-6;
	myfloat_t LAgx=5e-6;	// Ag - 5 um
	myfloat_t LSix=12e-6;   // Si - 12 um
	myfloat_t LInSbx=18e-6; // InSb - 18 um

	//source parameters
	myfloat_t Lsourcez=100e-6;  // 100 um from the left
	myfloat_t Lsourcex=6e-6;	// 6 um above Si/InSb interface

	////FDTD domain variables
	int Nwz = (int)(Lwz/dz);
	int NAgz = (int)(LAgz/dz);
	int NAgx = (int)(LAgx/dx);
	int NSix = (int)(LSix/dx);	
	int NInSbx = (int)(LInSbx/dx);

	int NAgxdown = NAgx;
	int NAgxup = NSix + NInSbx + NAgx;
	int NInSbxup = NInSbx + NAgx;
	int Nx = NSix + NInSbx + 2*NAgx;
	int Nz = Nwz + NAgz;
	printf("dx: %e = %f um \n", dx,  dx*1e6);
	printf("dz: %e = %f um \n", dz,  dz*1e6);
	printf("Nx=%i, Nz=%i\n", Nx, Nz);
	printf("Required waveguide length: %e: %i cells\n", Lwz, Nwz);
	printf("Computational domain size:\n");
	printf("dx*Nx: %e = %f um \n", dx*Nx, dx*Nx*1e6);
	printf("dz*Nz: %e = %f um \n\n", dz*Nz, dz*Nz*1e6);
	int t; // time step
	myfloat_t dt;

	// Hy mode - fileds
	myfloat_t *ex_field, *ez_field, *hy_field;
	myfloat_t *e_field_ampl;
	myfloat_t *jx_current, *jz_current; // ADE

	// InSb - fields in next time step
	myfloat_t *ex_field_next, *ez_field_next, *jx_current_next, *jz_current_next;

	// buff field pointers
	myfloat_t *ex_field_buff, *ez_field_buff, *jx_current_buff, *jz_current_buff;

	FILE *obs_fptr;
	char outbuffer[128];

	// compute dt based on cell size and Courant number
	dt = ((myfloat_t)(CFLFACTOR / c_const / sqrt(1.0 / dx / dx + 1.0 / dz / dz)));
	printf("Simulation time:\n");
	printf("dt: %e\n" "Nsteps: %i, "  "Nsteps*dt: %e\n" "time_e_ampl_save: %i, " "time_e_ampl_save*dt: %e\n\n", 
			dt,  	   Nsteps,		   Nsteps*dt, 		 time_e_ampl_save, 	  	  time_e_ampl_save*dt);

	////////  Hy mode - fields ////////
	ex_field = calloc(Nx*(Nz+1), sizeof(myfloat_t));
	ez_field = calloc((Nx+1)*Nz, sizeof(myfloat_t));
	hy_field = calloc(Nx*Nz, sizeof(myfloat_t));
	e_field_ampl = calloc(Nx*Nz, sizeof(myfloat_t));

	jx_current = calloc(Nx*(Nz+1), sizeof(myfloat_t));
	jz_current = calloc((Nx+1)*Nz, sizeof(myfloat_t)); // ADE

	//////// InSb - fields in next timestep //////// 
	ex_field_next = calloc(Nx*(Nz+1), sizeof(myfloat_t));
	ez_field_next = calloc((Nx+1)*Nz, sizeof(myfloat_t));
	jx_current_next = calloc(Nx*(Nz+1), sizeof(myfloat_t));
	jz_current_next = calloc((Nx+1)*Nz, sizeof(myfloat_t));


	//////// Magnetic line current source ////////
	myfloat_t *my_source = calloc(Nsteps, sizeof(myfloat_t));
	myfloat_t Tp = 0.5e-12;
	myfloat_t wp = 2*pi_const/Tp;
	
	myfloat_t t0 = 15*Tp; // TS
	myfloat_t tau = 6.4*Tp;
	//myfloat_t t0 = 10*Tp; // Shen
	//myfloat_t tau = 4*Tp;
	myfloat_t f0 = 1.5e12; // 1.5 THz

	printf("Source parameters: tau/dt: %e,  " "100Tp/dt: %e, " "t0/dt=%e\n\n",  
							   tau/dt, 		  (100*Tp)/dt, 		t0/dt);
	// Source in time
	for(int t1 = 0; t1 < Nsteps; t1++)
		my_source[t1] = exp(-pow(t1*dt-t0, 2.0)/pow(tau, 2.0)) * cos(-2*pi_const*f0*t1*dt);

	int k_source = (int)(Lsourcez/dz);
	int i_source = (int)(Lsourcex/dx) + NInSbx + NAgx;
	// k_source = Nz/2;
	printf("Source position: i: %d, k: %d\n", i_source, k_source);

	//////// Materials ////////

	// Si
	myfloat_t eps_si = 11.68*eps0_const;
	
	// Ag
	myfloat_t w_pe = 1.367e16;
	myfloat_t wt = 2.733e13; 
	//myfloat_t eps_inf_ag = 1.0;

	myfloat_t kp = (1 - wt*dt/2) / (1 + wt*dt/2);
	myfloat_t bp = (w_pe*w_pe*eps0_const*dt/2) / (1 + wt*dt/2);

	myfloat_t C1 = (2*eps0_const - dt*bp) / (2*eps0_const + dt*bp);
	myfloat_t C2 = (2*dt) / (2*eps0_const + dt*bp); // ADE

	//////// InSb - matrices ////////
	myfloat_t wc = 0.2*wp;
	myfloat_t v_insb = 0.001*wp;
	myfloat_t eps_inf_insb = 15.6;
	myfloat_t C[2][2];
	C[0][0] = v_insb;
	C[0][1] = wc;
	C[1][0] = -wc;
	C[1][1] = v_insb;
	myfloat_t d = eps0_const*eps_inf_insb*wp*wp;
#ifdef PRINT_MATRICES
	printf("C:\n");
	printf("condition number: %e\n", MatrixCondNumber(C));
	MatrixPrint(C);
#endif
	//////// Matrices calculations ////////
	// ===============================================
	myfloat_t I[2][2];
	myfloat_t Temp1[2][2];
	myfloat_t Temp2[2][2];
		
	I[0][0] = 1.0; I[0][1] = 0; I[1][0] = 0; I[1][1] = 1.0;

	// For J^n+1
	myfloat_t j_J[2][2];
	MatrixMultScalar(I, 2.0/dt, Temp1); MatrixAdd(Temp1, C, Temp1); MatrixInverse(Temp1, Temp2); // Temp2 = (2/dt*I + C)^-1
	MatrixMultScalar(I, 2.0/dt, Temp1); MatrixSub(Temp1, C, Temp1); // Temp1 = (2/dt*I - C)
	MatrixMulitply(Temp2, Temp1, j_J); // j_J = [(2/dt*I + C)^-1 * (2/dt*I - C)]
#ifdef PRINT_MATRICES
	printf("j_J:\n");
	printf("condition number: %e\n", MatrixCondNumber(j_J));
	MatrixPrint(j_J);
#endif
	myfloat_t j_E[2][2]; // j update from E field
	MatrixMultScalar(I, 2.0/dt, Temp1); MatrixAdd(Temp1, C, Temp1); MatrixInverse(Temp1, Temp2); // Temp2 = (2/dt*I + C)^-1
	MatrixMultScalar(Temp2, d, j_E); // j_E = d * (2/dt*I + C)^-1
#ifdef PRINT_MATRICES
	printf("j_E:\n");
	printf("condition number: %e\n", MatrixCondNumber(j_E));
	MatrixPrint(j_E);
	// J^n+1 = j_J * J^n  +  j_E * (E^n + E^n+1) //TS doubts...
#endif
	// For E^n+1
	myfloat_t F[2][2];
	MatrixMultScalar(I, 2.0/dt, Temp1); MatrixAdd(Temp1, C, Temp1); MatrixInverse(Temp1, Temp2); // Temp2 = (2/dt*I + C)^-1
	MatrixMultScalar(I, 2.0/dt, Temp1); MatrixSub(Temp1, C, Temp1); // Temp1 = (2/dt*I - C)
	MatrixMulitply(Temp2, Temp1, F); MatrixAdd(F, I, F); MatrixMultScalar(F, 0.5, F); // F = 0.5*[(2/dt*I + C)^-1 * (2/dt*I - C) + I]
#ifdef PRINT_MATRICES	
	printf("F:\n");
	printf("condition number: %e\n", MatrixCondNumber(F));
	MatrixPrint(F);
#endif
	myfloat_t G[2][2];
	MatrixMultScalar(I, 2.0/dt, Temp1); MatrixAdd(Temp1, C, Temp1); MatrixInverse(Temp1, Temp2); // Temp2 = (2/dt*I + C)^-1
	MatrixMultScalar(Temp2, d/2.0, G); // G = d/2 * (2/dt*I + C)^-1
#ifdef PRINT_MATRICES		
	printf("%e\n\n", d);
	printf("G:\n");
	printf("condition number: %e\n", MatrixCondNumber(G));
	MatrixPrint(G);
#endif	
	myfloat_t eps_matr[2][2];
	myfloat_t e_E[2][2];
	myfloat_t e_J[2][2];
	myfloat_t e_H[2][2];
	MatrixMultScalar(I, eps0_const*eps_inf_insb, eps_matr); // eps_matr = I*eps0_const*eps_inf_insb
	MatrixMultScalar(eps_matr, 1.0/dt, Temp1);  MatrixAdd(Temp1, G, Temp2); MatrixInverse(Temp2, e_H); // Temp1 = 1/dt*eps_matr, Temp2 = (1/dt*eps_matr + G),  e_H = (1/dt*eps_matr + G)^-1
	MatrixSub(Temp1, G, Temp2); // Temp2 = (1/dt*eps_matr - G)
	MatrixMulitply(e_H, Temp2, e_E); // e_E = (1/dt*eps_matr + G)^-1 * (1/dt*eps_matr - G)
	MatrixMulitply(e_H, F, e_J); // e_J = (1/dt*eps_matr + G)^-1 * F
	// E^n+1 = e_E * E^n + e_H * rot(H) -  e_J * J^n
#ifdef PRINT_MATRICES	
	printf("e_E:\n");
	printf("condition number: %e\n", MatrixCondNumber(e_E));
	MatrixPrint(e_E);
	printf("e_H:\n");
	printf("condition number: %e\n", MatrixCondNumber(e_H));
	MatrixPrint(e_H);
	printf("e_J:\n");
	printf("condition number: %e\n", MatrixCondNumber(e_J));
	MatrixPrint(e_J);
#endif
	// ===============================================
	
	// Save source
	if ((obs_fptr = fopen(".\\results\\source.bin", "wb")) == NULL)
		FdtdCpuError("Main, cannot open source.bin file");
	fwrite(my_source, sizeof(myfloat_t), Nsteps, obs_fptr);
	fclose(obs_fptr);

	//////// Main time loop ////////
	printf("Simulation begins\n");
#ifdef _OPENMP
	start = omp_get_wtime();
#else
	start = clock();
#endif
	
	for (t = 1; t < Nsteps; t++)
	{	
		//////// H-field update ////////
		Hyfield2DFdtdUpdate(dx, dz, dt,
			ex_field, ez_field, hy_field,
			Nx, Nz);

		//////// Add source - My current ////////
		hy_field[k_source*Nx + i_source] -= (dt/ dx/ dz/ mu0_const)* my_source[t];

		//////// save E amplitude plane in file ////////
		if (save_to_file(t, time_e_ampl_save))
		{
			// Calculate amplitude
			for (int k = 0; k < Nz; k++)
			{
				for (int i = 0; i < Nx; i++)
				{
					e_field_ampl[k*Nx+i] = sqrt(pow(ex_field[k*(Nx)+i], 2.0) + pow(ez_field[k*(Nx+1)+i], 2.0));
				}
			}
			
			// Save e amplitude to binary
			sprintf(outbuffer, ".\\results\\e_ampl_steps%d.bin", t);
			FdtdCpuSavePlaneToFile(outbuffer, e_field_ampl, Nx, Nz);

			fprintf(stderr, "Iteration: %d\r", t);
		}
				
		//////// E-field and J-current update ////////

		// Calculate E-field and J values in next timestep
		Exzfield2DFdtdUpdate(dx, dz, dt,
			ex_field, ez_field, hy_field,
			Nx, Nz,
			Nwz, NInSbxup, NAgxup, NAgxdown,
			eps_si,
			jx_current, jz_current, kp, bp,
			C1, C2,
			ex_field_next, ez_field_next, jx_current_next, jz_current_next,
			j_J, j_E, e_E, e_J, e_H							
			);
		// Copy newly calculated fields (swap pointers)
		ex_field_buff = ex_field;
		jx_current_buff = jx_current;
		ez_field_buff = ez_field;
		jz_current_buff = jz_current;

		ex_field = ex_field_next;
		jx_current = jx_current_next;
		ez_field = ez_field_next;
		jz_current = jz_current_next;

		ex_field_next = ex_field_buff;
		jx_current_next = jx_current_buff;
		ez_field_next = ez_field_buff;
		jz_current_next = jz_current_buff;

	}
	
#ifdef _OPENMP
	end = omp_get_wtime();
	cpu_time_used = end - start;
#else
	end = clock();
	cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;
#endif

	printf("Update time: %lf sec\n", cpu_time_used);
	printf("Processing speed: %lf Mcells/sec\n", (Nx*Nz) / cpu_time_used * 1e-6*Nsteps);
	
	//////// Save all fields at the end of simulation ////////
	sprintf(outbuffer, ".\\results\\e_x_steps%d.bin", Nsteps);
	FdtdCpuSavePlaneToFile(outbuffer, ex_field, Nx, Nz+1);

	sprintf(outbuffer, ".\\results\\e_z_steps%d.bin", Nsteps);
	FdtdCpuSavePlaneToFile(outbuffer, ez_field, Nx+1, Nz);

	sprintf(outbuffer, ".\\results\\h_y_steps%d.bin", Nsteps);
	FdtdCpuSavePlaneToFile(outbuffer, hy_field, Nx, Nz);

	sprintf(outbuffer, ".\\results\\j_x_steps%d.bin", Nsteps);
	FdtdCpuSavePlaneToFile(outbuffer, jx_current, Nx, Nz+1);

	sprintf(outbuffer, ".\\results\\j_z_steps%d.bin", Nsteps);
	FdtdCpuSavePlaneToFile(outbuffer, jz_current, Nx+1, Nz);
	

	//////// Release memory ////////
	free(ex_field);
	free(ez_field);
	free(hy_field);
	free(jx_current);
	free(jz_current);

	free(ez_field_next);
	free(ex_field_next);
	free(jx_current_next);
	free(jz_current_next);

	free(my_source);

	getchar();
	return 0;
}
