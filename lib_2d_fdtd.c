/******************************************************************************
 * 2-D FDTD library                               *
 * \file lib_2d_fdtd.c
 *****************************************************************************/

#include "lib_2d_fdtd.h"
#include "settings.h"

/** Print error message on the stderr and exit the code.
 */
void FdtdCpuError(const char *ptr_message)
{
	fprintf(stderr, "ERROR: ");
	fprintf(stderr, "%s", ptr_message);
	fprintf(stderr, "\n");
	getchar();
	exit(1);
}

 
/*****************************************************************************/
/* Saving data                                                               */
/*****************************************************************************/

/** Save plane to file, overwrite if file already exists.
 * \param filename name of file
 * \param data pointer to the array with data
 * \param dimx the first dimmension of the plane
 * \param dimz the second dimmension of the plane
 */
void FdtdCpuSavePlaneToFile(const char *filename,
							const myfloat_t *data,
							const uint_t dimx,
							const uint_t dimz)
{
	FILE *fptr;
	uint_t dim_tmp;

	if ((fptr = fopen(filename, "wb")) == NULL)
		FdtdCpuError("FdtdCpuSavePlaneToFile, cannot open file");

	// Writing dimensions of simulation domain to file
	dim_tmp = dimz;
	fwrite(&dim_tmp, sizeof(uint_t), 1, fptr);
	dim_tmp = dimx;
	fwrite(&dim_tmp, sizeof(uint_t), 1, fptr);

	fwrite(data, sizeof(myfloat_t), dimx*dimz, fptr);
	fclose(fptr);
}

/** Read plane from file.
 * \param filename name of file
 * \param data pointer to the array where data are saved
 * \param dimx the first dimmension of the plane
 * \param dimz the second dimmension of the plane
 */
void FdtdCpuReadPlanefromFile(const char *filename,
							  const myfloat_t *data,
							  const uint_t dimx,
							  const uint_t dimz)
{
	FILE *fptr;
	//uint_t dim_tmp;

	if ((fptr = fopen(filename, "rb")) == NULL)
	{
		perror("Error");
		FdtdCpuError("FdtdCpuReadPlanefromFile, cannot open file");
	}

	fread((myfloat_t *)data, sizeof(myfloat_t), dimx*dimz, fptr);
	fclose(fptr);
}


/*****************************************************************************/
/* 2-D TE mode (Hy mode) functions                                                        */
/*****************************************************************************/


/** 2-D TE (Hy mode) FDTD H-field update without
 * \param dx spatial step size along X-direction
 * \param dz spatial step size along Z-direction
 * \param dt time step size
 * \param ex_field array of Ex field
 * \param ez_field array of Ez field
 * \param hy_field array of Hy field
 * \param Nx number of cells along X-direction
 * \param Nz number of cells along Z-direction
 */
void Hyfield2DFdtdUpdate(const myfloat_t dx, const myfloat_t dz, const myfloat_t dt,
						myfloat_t *ex_field, myfloat_t *ez_field, myfloat_t *hy_field,
						int Nx, int Nz)
{
	int i, k;
	int kex, kez, khy;
	int ikex, ikez, ikhy;

	//update HY
#ifdef _OPENMP
	#pragma omp parallel for private(i, khy, kex, kez, ikhy, ikex, ikez)
#endif
	for (k = 0; k < Nz; k++)
	{
		khy = k * (Nx);
		kex = k * (Nx);
		kez = k * (Nx + 1);
		for (i = 0; i < Nx; i++)
		{
			ikhy = i + khy; //HY pointer
			ikex = i + kex; //EX pointer
			ikez = i + kez; //Ez pointer

			hy_field[ikhy] = hy_field[ikhy] +
				(dt / mu0_const)*(
				-(ex_field[ikex + (Nx)] - ex_field[ikex]) / dz
				+
				(ez_field[ikez + 1] - ez_field[ikez]) / dx
					);
		}
	}
}

/** 2-D TE (Hy mode) FDTD E-field update without, using ADE method
 * \param dx spatial step size along X-direction
 * \param dz spatial step size along Z-direction
 * \param dt time step size
 * \param ex_field array of Ex field
 * \param ez_field array of Ez field
 * \param hy_field array of Hy field
 * \param Nx number of cells along X-direction
 * \param Nz number of cells along Z-direction
 * \param Nwz waveguide length in cells
 * \param NInSbxup top of InSb in cells (interface between media)
 * \param NAgxup top of Ag
 * \param NAgxdown down of Ag
 * \param eps_si permittivity of Si
 * \param jx_current array of Jx current density
 * \param jz_current array of Jz current density
 * \param kp coefficient used in update of k current density (silver, ADE)
 * \param bp coefficient used in update of k current density (silver, ADE)
 * \param C1 coefficient used in update of E field (silver, ADE)
 * \param C2 coefficient used in update of E field (silver, ADE)
 * \param ex_field_next array of new values of Ex field
 * \param ez_field_next array of new values of Ez field
 * \param jx_current_next array of new values of Jx field
 * \param jz_current_next array of new values of Jz field
 * \param j_J matrix used in calculating J value based on J (InSb - anisotropic material)
 * \param j_E matrix used in calculating J value based on E (InSb - anisotropic material)
 * \param e_E matrix used in calculating E value based on E (InSb - anisotropic material)
 * \param e_H matrix used in calculating E value based on J (InSb - anisotropic material)
 */ 
void Exzfield2DFdtdUpdate(const myfloat_t dx, const myfloat_t dz, const myfloat_t dt,
						  myfloat_t *ex_field, myfloat_t *ez_field, myfloat_t *hy_field,
						  int Nx, int Nz,
						  int Nwz, int NInSbxup, int NAgxup, int NAgxdown,
						  const myfloat_t eps_si,
						  myfloat_t *jx_current, myfloat_t *jz_current, const myfloat_t kp, const myfloat_t bp, 
						  const myfloat_t C1, const myfloat_t C2,
						  myfloat_t *ex_field_next, myfloat_t *ez_field_next, myfloat_t *jx_current_next, myfloat_t *jz_current_next,
						  myfloat_t j_J[2][2], myfloat_t j_E[2][2], myfloat_t e_E[2][2], myfloat_t e_J[2][2], myfloat_t e_H[2][2]
						 )
{
	int i, k;
	int kex, kez, khy;
	int ikex, ikez, ikhy;

	//update Ex and Jx
#ifdef _OPENMP
	#pragma omp parallel for private(i, khy, kex, ikhy, ikex)
#endif
	for (k = 1; k < Nz; k++)
	{
		khy = k * Nx;
		kex = k * Nx;
		for (i = 0; i < Nx; i++)
		{
			ikhy = i + khy; //HY pointer
			ikex = i + kex; //EX pointer
			
			if(is_Ag(i, k, Nwz, NAgxup, NAgxdown)) //ADE - silver
			{
				myfloat_t jx_current_temp = kp*jx_current[ikex] + bp*ex_field[ikex]; // New Jx value, first stage(based on old E value - E(t=n));
				ex_field_next[ikex] = C1*ex_field[ikex] + C2*( -(hy_field[ikhy] - hy_field[ikhy-Nx]) / dz - 0.5*(1+kp)*jx_current[ikex] );
				jx_current_next[ikex] = jx_current_temp + bp*ex_field_next[ikex]; // Update Jx, second stage (based on new E value - E(t=n+1))
			}
			else if(is_InSb(i, k, Nwz, NInSbxup, NAgxdown)) // InSb
			{
				myfloat_t jz_pos_x = 0.25*(jz_current[k*(Nx+1)+i] + jz_current[(k-1)*(Nx+1)+i] + jz_current[k*(Nx+1)+i+1] + jz_current[(k-1)*(Nx+1)+i+1]); // Jz current in position (i, k+1/2) - average value of neighbour currents
				myfloat_t ez_pos_x = 0.25*(ez_field[k*(Nx+1)+i] + ez_field[(k-1)*(Nx+1)+i] + ez_field[k*(Nx+1)+i+1] + ez_field[(k-1)*(Nx+1)+i+1]); // Ez field in position (i, k+1/2)

				jx_current_next[ikex] = j_J[0][0]*jx_current[ikex] + j_J[0][1]*jz_pos_x  +  
										j_E[0][0]*ex_field[ikex] + j_E[0][1]*ez_pos_x; // Jx update: Jx^n, Ex^n, Ez^n components 
				
				ex_field_next[ikex] = e_E[0][0]*ex_field[ikex] + e_E[0][1]*ez_pos_x + 
									  e_H[0][0]*(-(hy_field[ikhy] - hy_field[ikhy-Nx]) / dz) + 
									  0.25*e_H[0][1]*((hy_field[ikhy - Nx + 1] - hy_field[ikhy - Nx - 1] + hy_field[ikhy + 1] - hy_field[ikhy - 1]) / dx) -
									  (e_J[0][0]*jx_current[ikex] + e_J[0][1]*jz_pos_x);

				jx_current_next[ikex] += j_E[0][0] * ex_field_next[ikex]; // Jx update: Ex^n+1 component
			}
			else // Si
			{
				ex_field_next[ikex] = ex_field[ikex] +
					(dt / eps_si)*(
					-(hy_field[ikhy] - hy_field[ikhy-Nx]) / dz
						);
			}
		}
	}

	//update Ez and Jz
#ifdef _OPENMP
	#pragma omp parallel for private(i, khy, kez, ikhy, ikez)
#endif
	for (k = 0; k < Nz; k++)
	{
		khy = k * Nx;
		kez = k * (Nx+1);
		for (i = 1; i < Nx; i++)
		{			
			ikhy = i + khy; //HY pointer
			ikez = i + kez; //EZ pointer

			if(is_Ag(i, k, Nwz, NAgxup, NAgxdown)) //ADE - silver
			{	
				myfloat_t jz_current_temp = kp*jz_current[ikez] + bp*ez_field[ikez]; // New Jz value, first stage(based on old E value- E(t=n));
				ez_field_next[ikez] = C1*ez_field[ikez] + C2*( (hy_field[ikhy] - hy_field[ikhy-1]) / dx - 0.5*(1+kp)*jz_current[ikez] );
				jz_current_next[ikez] = jz_current_temp + bp*ez_field_next[ikez]; // Update Jz, second stage (based on new E value - E(t=n+1))
			}
			else if(is_InSb(i, k, Nwz, NInSbxup, NAgxdown)) // InSb
			{
				myfloat_t jx_pos_z = 0.25*(jx_current[k*Nx+i] + jx_current[k*Nx+i-1] + jx_current[(k+1)*Nx+i] + jx_current[(k+1)*Nx+i-1]); // Jx current in position (i, k+1/2) - average value of neighbour currents
				myfloat_t ex_pos_z = 0.25*(ex_field[k*Nx+i] + ex_field[k*Nx+i-1] + ex_field[(k+1)*Nx+i] + ex_field[(k+1)*Nx+i-1]);  // Ex field in position (i, k+1/2)

				jz_current_next[ikez] = j_J[1][0]*jx_pos_z + j_J[1][1]*jz_current[ikez] +  
										j_E[1][0]*ex_pos_z + j_E[1][1]*(ez_field[ikez]); // Jz update: Jz^n, Ex^n, Ez^n components 
				
				if (k !=0 )
					ez_field_next[ikez] = e_E[1][0]*ex_pos_z + e_E[1][1]*ez_field[ikez] -
									  0.25*e_H[1][0]*((hy_field[ikhy + Nx - 1] - hy_field[ikhy - Nx - 1] + hy_field[ikhy + Nx] - hy_field[ikhy - Nx]) / dz) +  
									  e_H[1][1]*((hy_field[ikhy] - hy_field[ikhy-1]) / dx) -
									  (e_J[1][0]*jx_pos_z + e_J[1][1]*jz_current[ikez]);
				else //left boundary
					ez_field_next[ikez] = e_E[1][0]*ex_pos_z + e_E[1][1]*ez_field[ikez] -
									  0.25*e_H[1][0]*((hy_field[ikhy + Nx] - hy_field[ikhy] + hy_field[ikhy + Nx - 1] - hy_field[ikhy - 1]) / dz) +  
									  e_H[1][1]*((hy_field[ikhy] - hy_field[ikhy-1]) / dx) -
									  (e_J[1][0]*jx_pos_z + e_J[1][1]*jz_current[ikez]);

				jz_current_next[ikez] += j_E[1][1] * ez_field_next[ikez]; // Jx update: Ez^n+1 component
			}
			else // Si
			{
				ez_field_next[ikez] = ez_field[ikez] +
					(dt / eps_si)*(
					(hy_field[ikhy] - hy_field[ikhy-1]) / dx
						);
			}
		} 
	}

	// Finish updating Jx and Jz (with respectively Ez^n+1 and Ex^n+1)
/*
	for (k = 1; k < Nz-1; k++)
	{
		for (i = 1; i < Nx - 1; i++)
		{
			if(is_InSb(i, k, Nwz, NInSbxup, NAgxdown))
			{
				jx_current_next[k*(Nx+1)+i] += j_E[0][1] * 0.25*(ez_field_next[k*Nx+i] + ez_field_next[(k-1)*Nx+i] + 
																 ez_field_next[k*Nx+i+1] + ez_field_next[(k-1)*Nx+i+1]);
				jz_current_next[k*Nx+i] 	+= j_E[1][0] * 0.25*(ex_field_next[k*(Nx+1)+i] + ex_field_next[k*(Nx+1)+i-1] +
																 ex_field_next[(k+1)*(Nx+1)+i] + ex_field_next[(k+1)*(Nx+1)+i-1]);
			}
		}
	}
*/
	// Finish updating Jx (with Ez^n+1)
#ifdef _OPENMP
	#pragma omp parallel for private(i, khy, kex, ikhy, ikex)
#endif
	for (k = 1; k < Nz; k++)
	{
		khy = k * Nx;
		kex = k * Nx;
		for (i = 0; i < Nx; i++)
		{
			ikhy = i + khy; //HY pointer
			ikex = i + kex; //EX pointer
			if(is_InSb(i, k, Nwz, NInSbxup, NAgxdown)) // InSb
			{
				myfloat_t ez_pos_x = 0.25*(ez_field_next[k*(Nx+1)+i] + ez_field_next[(k-1)*(Nx+1)+i] + ez_field_next[k*(Nx+1)+i+1] + ez_field_next[(k-1)*(Nx+1)+i+1]); // Ez field (next) in position (i, k+1/2)
				jx_current_next[ikex] += j_E[0][1] * ez_pos_x; // Jx update: Ez^n+1 component
			}
		}
	}
	// Finish updating Jz (with Ex^n+1)
#ifdef _OPENMP
	#pragma omp parallel for private(i, khy, kez, ikhy, ikez)
#endif
	for (k = 0; k < Nz; k++)
	{
		khy = k * Nx;
		kez = k * (Nx+1);
		for (i = 1; i < Nx; i++)
		{			
			ikhy = i + khy; //HY pointer
			ikez = i + kez; //EZ pointer
			if(is_InSb(i, k, Nwz, NInSbxup, NAgxdown)) // InSb
			{
				myfloat_t ex_pos_z = 0.25*(ex_field_next[k*Nx+i] + ex_field_next[k*Nx+i-1] + ex_field_next[(k+1)*Nx+i] + ex_field_next[(k+1)*Nx+i-1]);  // Ex field in position (i, k+1/2)
				jz_current_next[ikez] += j_E[1][0] * ex_pos_z; // Jz update: Ex^n+1 component
			}
		}
	}
}

/*****************************************************************************/
/* 2x2 Matrices functions                                                      */
/*****************************************************************************/
/* Checked by TS */

/** Inverse 2x2 matrix
 * \param A original 2x2 matrix (2x2 array)
 * \param result inverse of A
 */
void MatrixInverse(myfloat_t A[2][2], myfloat_t result[2][2])
{

	myfloat_t det = A[0][0]*A[1][1] - A[1][0]*A[0][1];
	result[0][0] = 1/det*A[1][1];
	result[1][1] = 1/det*A[0][0];
	result[0][1] = 1/det*(-A[0][1]);
	result[1][0] = 1/det*(-A[1][0]);
}

/** Addition of two 2x2 matrices
 * \param A first 2x2 matrix (2x2 array)
 * \param B second 2x2 matrix (2x2 array)
 * \param result A + B
 */
void MatrixAdd(myfloat_t A[2][2], myfloat_t B[2][2],  myfloat_t result[2][2])
{
	result[0][0] = A[0][0] + B[0][0];
	result[0][1] = A[0][1] + B[0][1];
	result[1][0] = A[1][0] + B[1][0];
	result[1][1] = A[1][1] + B[1][1];
}

/** Substraction of two 2x2 matrices
 * \param A first 2x2 matrix (2x2 array)
 * \param B second 2x2 matrix (2x2 array)
 * \param result A - B
 */
void MatrixSub(myfloat_t A[2][2], myfloat_t B[2][2],  myfloat_t result[2][2])
{
	result[0][0] = A[0][0] - B[0][0];
	result[0][1] = A[0][1] - B[0][1];
	result[1][0] = A[1][0] - B[1][0];
	result[1][1] = A[1][1] - B[1][1];
}


/** Multiplication of two 2x2 matrices (does not work in place)
 * \param A first 2x2 matrix (2x2 array)
 * \param B second 2x2 matrix (2x2 array)
 * \param result A * B
 */
void MatrixMulitply(myfloat_t A[2][2], myfloat_t B[2][2],  myfloat_t result[2][2])
{
	result[0][0] = A[0][0]*B[0][0] + A[0][1]*B[1][0];
	result[0][1] = A[0][0]*B[0][1] + A[0][1]*B[1][1];
	result[1][0] = A[1][0]*B[0][0] + A[1][1]*B[1][0];
	result[1][1] = A[1][0]*B[0][1] + A[1][1]*B[1][1];
}

/** Multiplication of two 2x2 matrix by scalar
 * \param A first 2x2 matrix (2x2 array)
 * \param scalar scalar to multiply A by
 * \param result scalar * A
 */
void MatrixMultScalar(myfloat_t A[2][2], myfloat_t scalar,  myfloat_t result[2][2])
{
	result[0][0] = scalar*A[0][0];
	result[0][1] = scalar*A[0][1];
	result[1][0] = scalar*A[1][0];
	result[1][1] = scalar*A[1][1];
}

/** Returns condition number of matrix
 * \param A first 2x2 matrix (2x2 array)
 */
myfloat_t MatrixCondNumber(myfloat_t A[2][2])
{
	return (pow(A[0][0], 2.0) + pow(A[0][1], 2.0) + pow(A[1][0], 2.0) + pow(A[1][1], 2.0)) / (A[0][0]*A[1][1] - A[0][1]*A[1][0]);
}

/** Print 2x2 matrix
 * \param A  2x2 matrix (2x2 array)
 */
void MatrixPrint(myfloat_t A[2][2])
{
	printf("|%e %e| \n|%e %e|\n\n", A[0][0], A[0][1], A[1][0], A[1][1]);
}
