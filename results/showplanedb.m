
function plane=showplanedb(filename)




fptr=fopen(filename);
planesize=fread(fptr,2,'uint');
Nz=planesize(1);
Nx=planesize(2);

figure(1);
%Nx = 1501;
%Nz = 10006;
% Nx = 1501;
% Nz = 40027;
field_type = filename(1:3);
if strcmp(field_type, 'e_x') || strcmp(field_type, 'j_x')
    Nx = Nx+1
elseif strcmp(field_type, 'e_z') || strcmp(field_type, 'j_z')
    Nz = Nz+1
end
    
plane=fread(fptr,[Nx, Nz],'double');

size(plane)
mesh(plane);

% plot lines at boundaries
% hold on
% x = [0, Nz-50]; y = [900, 900];
% plot(x, y, 'b','LineWidth',0.5)
% x = [Nz-50, Nz-50]; y = [0, Nx];
% plot(x, y, 'b','LineWidth',0.5)
% hold off
% 
% view(0,90);
% xlim([1 Nz+1]);
% ylim([1 Nx+1]);
% caxis([-1e-9 1e-9]);
title('E field amplitude');



xlabel('z (cells)');
ylabel('x (cells)');


% % % Linear
colorbar;
colormap(hsv);


fclose(fptr);
