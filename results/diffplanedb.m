% Difference plotter developed as a part of the project:

% Advanced Simulation Methods for Electromagnetic Exposure Assessment

% supported by Foundation for Polish Science 2011-2013


function difference=diffplanedb(filename1, filename2)

fptr1=fopen(filename1);
fptr2=fopen(filename2);%reference point

planesize=fread(fptr1,2,'uint');
nx=planesize(1);
ny=planesize(2);
planesize=fread(fptr2,2,'uint');
mx=planesize(1);
my=planesize(2);

if (nx~=mx || ny~=my)
	error('wrong data size!');
end

array=fread(fptr1,[nx,ny],'double');
reference=fread(fptr2,[nx,ny],'double');

max_ref=max(max(abs(reference)));
c=100 * abs(array-reference)/max_ref;
difference=[max(max(c)) 100*norm(array-reference)/norm(reference)];

%max_ref=max(max(abs(reference)));
%c=100 * abs(array-reference)/max_ref;
%difference=max(max(c));

mesh(c);
title('2D error plane');
xlabel('x (cells)');
ylabel('y (cells)');

fclose(fptr1);
fclose(fptr2);
