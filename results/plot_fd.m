
function plotwaveformdb_fd(filename)

% Calculate timestep etc
eps0_const=8.85418781762e-12;
mu0_const=1.2566371e-6;
%eta0_const=sqrt(eps0_const / mu0_const);
c_const=1.0/sqrt(eps0_const * mu0_const);
%pi_const=3.14159265358979323846264338327950288;
CFLFACTOR=0.99;

% Data from file
fptr=fopen(filename);
[array, nx]=fread(fptr,'double');

fmax=750e12;
%ADE
% fmin=0.6e16;
% fmax=0.7e16;

%fcentral=(fmin+fmax)/2;

%lambda_max=c_const/fmin;
lambda_min=c_const/fmax;
%lambda_central=c_const/fcentral;

steps_per_wavelength=20;

dx=lambda_min/steps_per_wavelength;
dy=dx;

dt=CFLFACTOR / c_const / sqrt(1.0 / dx / dx + 1.0 / dy / dy);

Nsteps = nx
t=[0:1:Nsteps];
simtime=t*dt;

% FFT
Nplot=Nsteps;
Fs = 1.0/dt;
y_fft=fft(array(1:Nplot));

P2 = abs(y_fft/Nplot);
P1 = P2(1:Nplot/2+1);
P1(2:end-1) = 2*P1(2:end-1);

% Frequency domain
f = Fs*(0:(Nplot/2))/Nplot;

% Expected result
f0 = 1.5e12;
df = 0.1e12;
A0 = max(P1); % Assuming max value of caclulated fft is expected one
A = A0*exp(-(f-f0).^2 / df.^2);

plot(f, A);
hold on
plot(f,P1);
hold off

legend("expected fft", "fft of source");
xlabel('f (Hz)');
ylabel('|E_z(f)|');


% xlim([0.2*fmin 1.1*fmax]);

fclose(fptr);
