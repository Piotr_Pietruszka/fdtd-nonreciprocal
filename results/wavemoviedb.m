function wavemoviedb = wavemoviedb()



v = VideoWriter('a_simulation_result.avi','Uncompressed AVI');
v.FrameRate = 10;
open(v)
Nsteps = 100000;
% for t=0:1:320-1; %  t=0:1:20-1; % t=10200:600:Nsteps-1
% for t=10200:600:Nsteps-1;
for t=5000:5000:Nsteps-1;
%     filename=strcat('h_y_steps', num2str(t), '.bin');
    filename=strcat('e_ampl_steps', num2str(t), '.bin');
    showplanedb(filename);
    frame = getframe(gcf);
    writeVideo(v, frame);
    disp(filename);
end

close(v);