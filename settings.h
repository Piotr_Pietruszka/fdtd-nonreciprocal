#ifndef SETTINGS_H_INCLUDED
#define SETTINGS_H_INCLUDED

#ifndef _OPENMP
#define _OPENMP
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>



/*****************************************************************************/
/* Constants                    */
/*****************************************************************************/

#define eps0_const 8.85418781762e-12
#define mu0_const  1.2566371e-6
#define eta0_const ((myfloat_t)(sqrt(mu0_const / eps0_const)))
#define c_const    ((myfloat_t)(1.0/sqrt(eps0_const * mu0_const)))
#define pi_const   3.14159265358979323846264338327950288

#define CFLFACTOR  0.99 //CFL factor

/*****************************************************************************/
/* Material boundaries                    */
/*****************************************************************************/
// k > 1 below is for further consideration!!!
#define is_InSb(i, k, Nwz, NInSbxup, NAgxdown) (k < Nwz &&  i < NInSbxup && i >= NAgxdown) // InSb bottom part of waveguide
//#define is_InSb(i, k, Nwz, NInSbxup, NAgxdown) (k <= Nwz  && k > 1  &&  i < NInSbxup && i >= NAgxdown) // InSb bottom part of waveguide
// #define is_InSb(i, k) (k < Nz-10  && k > 10  &&  i < Nx-10 && i > 10) // almost whole domain

#define is_Ag(i, k, Nwz, NAgxup, NAgxdown) (k >= Nwz || i >= NAgxup || i < NAgxdown) // Ag at right, top and bottom of domain
// #define is_Ag(i, k) ((k < 100 || i > Nx-10 || i < 10)) // Ag at left end of domain

/** File save macro */
// #define save_to_file(t, t_mod, t0, t1) (t%t_mod==0 || (t > t0 && t < t1) )
#define save_to_file(t, t_mod) (t%t_mod==0)

// #define PRINT_MATRICES

/*****************************************************************************/
/* Data types                                */
/*****************************************************************************/

/** Floating point */
typedef double myfloat_t;

/** Unsigned integer */
typedef unsigned int uint_t;


#endif
