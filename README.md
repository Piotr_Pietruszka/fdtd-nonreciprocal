﻿
## Setup compilation environment
##### Install gcc complier with OpenMP (e.g. from: https://winlibs.com/?fbclid=IwAR1jrvMTsc15ET-UnaWBGN5yjTSy0s7LwIsY2T23q9XHwxlPDM1MEqkPJQ0#download-release)
- download zip file (e.g. UCRT runtime - Win64: https://github.com/brechtsanders/winlibs_mingw/releases/download/11.2.0-10.0.0-ucrt-r1/winlibs-x86_64-posix-seh-gcc-11.2.0-mingw-w64ucrt-10.0.0-r1.zip)
- unpack it to chosen folder (e.g. C:\mingw64)
- add path (C:\mingw64\bin) to environment variables

Compile project by running in project folder: 

    gcc -g *.c -o project_2d_fdtd.exe -O3 -Wall -fopenmp

## Files
- settings.h - macros (boundaries of materials) and most physical constants definitions
- lib_2_fdtd.h - header file for functions
- lib_2_fdtd.c - functions definitions: H update, calculating new values of E and J, 2x2 matrix functions
- project_2_fdtd.c - main file.  Includes definitions of remaining constants (e.g. cell size, time step, material constants), source definition and main time loop.

All constants values are changed inside files.

## lib_2_fdtd.c
Hyfield2DFdtdUpdate - updating Hy field value. Same for whole domain (independent of material).
```C
hy_field[ikhy] = hy_field[ikhy] +
				(dt / mu0_const)*(
				-(ex_field[ikex + (Nx+1)] - ex_field[ikex]) / dz
				+
				(ez_field[ikez + 1] - ez_field[ikez]) / dx);
```

Exzfield2DFdtdUpdate -  calculating values at next time step for Ex, Ez, Jx and Jz. Update equations differ for different materials (Ag, InSb, Si)
## project_2_fdtd.c

Domain constants definitions:
```C
...
int  Nx = (int)(12e-6/dx + 18e-6/dx + 1e-6*dx); 
int  Nz = (int)(600e-6/dx); // 600 um
...
dt = ((myfloat_t)(CFLFACTOR / c_const / sqrt(1.0 / dx / dx + 1.0 / dz / dz)));
```

Material constants:
```C
// Si
myfloat_t  eps_si = 11.68*eps0_const;
// Ag
myfloat_t  w_pe = 1.367e16;
myfloat_t  wt = 2.733e13;
myfloat_t  eps_inf_ag = 1.0;
...

//////// InSb - matrices ////////
myfloat_t  wc = 0.2*wp;
myfloat_t  v_insb = 0.001*wp;
myfloat_t  eps_inf_insb = 15.6;
myfloat_t  C[2][2];
C[0][0] = v_insb;
...
```

Source definition
```C
for(int  t1 = 0; t1 < Nsteps; t1++)
	my_source[t1] = exp(-pow(t1*dt-t0, 2.0)/pow(tau, 2.0)) * cos(-2*pi_const*f0*t1*dt);
```

#### Inside time loop
- Hy field update
- Adding source 
	```C
	hy_field[k_source*Nx + i_source] -= (dt / mu0_const)* my_source[t];
	```
- 	Saving amplitude of E field (at chosen timesteps)
- Updating Ex, Ez, Jx, Jz
	```C
	Exzfield2DFdtdUpdate(...);
	memcpy(jx_current, jx_current_next, (Nx+1)*Nz*sizeof(myfloat_t)); 
	memcpy(ex_field, ex_field_next, (Nx+1)*Nz*sizeof(myfloat_t)); 
	memcpy(jz_current, jz_current_next, Nx*(Nz+1)*sizeof(myfloat_t)); 
	memcpy(ez_field, ez_field_next, Nx*(Nz+1)*sizeof(myfloat_t)); 
	```
## settings.h
Physical Constants definitions
```C
 #define  eps0_const  8.85418781762e-12
 ...
```
Material boundaries (rest is Si)
```C
#define  is_InSb(i, k) (k <= Nz-50  && k >  1  && i <  900  && i >=  10)
#define  is_Ag(i, k) ((k > Nz-50  || i > Nx-10  || i <  10))
```
	
